import Vue from 'vue'

const getDefaultState = () => {
	return {
		configurations: []
	}
}

const state = getDefaultState()

const getters = {
	configurations(state) {
		return state.configurations
	}
}

const actions = {
	listConfigurations({commit}) {
		return new Promise((resolve, reject) => {
			Vue.http.configuraciones()
			.then((res) => {
				commit('SET_CONFIGURATIONS', res.data.data)
				resolve()
			})
			.catch((err) => {
				commit('FAIL')
				reject(err.response.data)
			})
		})
	},
}

const mutations = {
	SET_CONFIGURATIONS (state, configurations) {
		state.configurations = configurations
	},
}

export default { state, getters, actions, mutations }