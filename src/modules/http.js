import { get, post, put, del } from './methodRequest'

export default function(Vue) {
	Vue.http = {
		login(data) {
			return post('login', data)
		},
		logout() {
			return post('logout')
		},
	}

	Object.defineProperties(Vue.prototype,{
		$http: {
			get() {
				return Vue.http
			}
		}

	})
}
