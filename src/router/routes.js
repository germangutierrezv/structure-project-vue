export default [
  {
    path: '/',
    component: () => import('../components/Layout/Login.vue'),
    children: [
      { path: '/login', component: () => import('../pages/Login/Index.vue') },
      { path: '/', redirect: '/login' }
    ]
  },
  {
    path: '/',
    component: () => import('../components/Layout/Complex.vue'),
    children: [
      { path: '/dashboard', component: () => import('../pages/Dashboard/Dashboard.vue') },
      { path: '/', redirect: '/dashboard' }
    ]
  }
]
